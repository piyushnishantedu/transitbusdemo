//
//  BaseViewController.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 12/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
let activityIndicatorView = ActivityIndicatorView(frame: UIScreen.main.bounds)
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureActivityIndicator()
        self.view.addSubview(activityIndicatorView)
    }
    private func configureActivityIndicator(){
        activityIndicatorView.showText(withText: nil)
        activityIndicatorView.isViewHidden = false
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
