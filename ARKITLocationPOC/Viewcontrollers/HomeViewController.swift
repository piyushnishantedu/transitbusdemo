//
//  HomeViewController.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 28/02/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
class HomeViewController: BaseViewController {
    
    
    @IBOutlet weak var map: MKMapView!
    fileprivate let locationManager = CLLocationManager()
    fileprivate var googlePlaces: [GooglePlace]?{
        didSet{
            //Drop pin to map
            setAnnotaionsOnMap()
        }
    }
    fileprivate var stops:[Stop]?{
        didSet{
            setStopAnnotaionsOnMap()
        }
    }
    
    var zoomLevel: Float = 15.0
    var currentLocation:CLLocation?{
        didSet{
            locationManager.stopUpdatingLocation()
            let span = MKCoordinateSpan(latitudeDelta: 0.014, longitudeDelta: 0.014)
            let region = MKCoordinateRegion(center: currentLocation!.coordinate, span: span)
            map.region = region
            // getGooglePlaces()
            getStops()
            
        }
    }
    fileprivate var arViewController: ARViewController!
    
    
    //fileprivate var currentSelectedStop:Stop?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        configureLOcationManager()
        getStops()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentLocation = CLLocation(latitude: 49.248523, longitude: -123.108800)
    }
    
    @IBAction func arViewButtonAction(_ sender: UIButton) {
        arViewController = ARViewController()
        arViewController.dataSource = self
        arViewController.presenter.maxVisibleAnnotations = 30
        arViewController.presenter.presenterTransform = ARPresenterStackTransform()
        guard let stops = self.stops else{return}
        arViewController.setAnnotations(stops)
        //HARDCODE Current LOCATION
        arViewController.trackingManager.currentFilterLocation = CLLocation(coordinate: CLLocationCoordinate2D(latitude: 49.24852, longitude:-123.108800), altitude: 95, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp: Date())
        self.navigationController?.pushViewController(arViewController, animated: true)
        
    }
    
    private func configureLOcationManager(){
        
        //tempTestLocation()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
    }
    fileprivate func getGooglePlaces(){
        GooglePlaceApiManager.getGooglePlaces(withLocation: currentLocation!) { (responseContainer) in
            if responseContainer.error != nil{
                //Code to show Error
            }else{
                self.googlePlaces = responseContainer.responseCollection
                //Code To Handel Response Objects
            }
        }
        
    }
    fileprivate func getStops(){
        let loc = CLLocation(latitude: 49.24852, longitude:123.108800)
        TransitStopManager.getStops(withLocation: loc) { (responseContainer) in
            if responseContainer.error != nil{
                //Code to show Error
                self.activityIndicatorView.showText(withText: responseContainer.error!.userInfo["error"] as? String)
            }else{
                self.stops = responseContainer.responseCollection
                //Code To Handel Response Objects
                self.activityIndicatorView.isViewHidden = true
            }
        }
    }
    
    //TEMP Code
    private func tempTestLocation(){
        let cord = CLLocationCoordinate2D(latitude: 28.4089, longitude: 77.3178)
        currentLocation = CLLocation(latitude: cord.latitude, longitude: cord.longitude)
    }
    private func setAnnotaionsOnMap(){
        guard let places = self.googlePlaces else {
            return
        }
        for place in places{
            let cordinate = CLLocationCoordinate2D(latitude: place.location.coordinate.latitude, longitude: place.location.coordinate.longitude)
            let annotation = PlaceAnnotation(location: cordinate, title: "My name")
            //6
            DispatchQueue.main.async {
                self.map.addAnnotation(annotation)
            }
        }
        
    }
    private func setStopAnnotaionsOnMap(){
        guard let places = self.stops else {
            return
        }
        for place in places{
            let cordinate = CLLocationCoordinate2D(latitude: place.location.coordinate.latitude, longitude: place.location.coordinate.longitude)
            let annotation = PlaceAnnotation(location: cordinate, title:place.name)
            //6
            DispatchQueue.main.async {
                self.map.addAnnotation(annotation)
            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    
}
extension HomeViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else{return}
        //currentLocation = location
        
        
        
        
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
extension HomeViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AnnotationView()
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        annotationView.updateUI()
        return annotationView
    }
    
}
extension HomeViewController: AnnotationViewDelegate {
    
    func didTouch(annotationView: ARAnnotationView) {
        let popOverVC = StopDetailPopViewController.instantiate(withStoryBoard:StoryboardName.main.rawValue)
        popOverVC.stop = annotationView.annotation as? Stop
        //currentSelectedStop = popOverVC.stop
        arViewController.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        arViewController.view.addSubview(popOverVC.view)
        popOverVC.currentLocation = self.currentLocation
        popOverVC.didMove(toParentViewController: self)
    }
}

