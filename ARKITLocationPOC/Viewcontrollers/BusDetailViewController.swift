//
//  BusDetailViewController.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 12/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit

class BusDetailViewController: UIViewController,PopoverStyle {

    @IBOutlet weak var contentMainView: UIView!
    
    @IBOutlet weak var vechileNumberInfo: UILabel!
    
    @IBOutlet weak var vechileNumber: UILabel!
    
    @IBOutlet weak var routeNumberInfo: UILabel!
    
    @IBOutlet weak var routeNumber: UILabel!
    
    @IBOutlet weak var directionInfo: UILabel!
    
    @IBOutlet weak var direction: UILabel!
    
    @IBOutlet weak var destinationInfo: UILabel!
    
    @IBOutlet weak var destination: UILabel!
    
    @IBOutlet weak var lastAddressinfo: UILabel!
    
    @IBOutlet weak var lastAddress: UILabel!
    
    @IBOutlet weak var recordedTimeInfo: UILabel!
    
    @IBOutlet weak var recordedTime: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    var bus:Bus?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        styleView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateView()
    }

    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    
    private func styleView(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        let layer = contentMainView.layer
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: contentMainView.bounds , cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        cancelButton.layer.cornerRadius = 10
    }
    private func updateView(){
        guard let bus = self.bus else{return}
        vechileNumber.text = bus.vehicleNo
        routeNumber.text = bus.routeNo
        direction.text = bus.direction
        destination.text = bus.destination
        recordedTime.text = bus.lastRecordedTime
        Helper.getAddressWith(latitdue: bus.latitude, longitude: bus.longitude) { [weak self] (address) in
            self?.lastAddress.text = address
        }
        
    }

}
