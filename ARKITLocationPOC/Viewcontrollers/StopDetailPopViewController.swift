//
//  StopDetailPopViewController.swift
//  POPPOC
//
//  Created by Piyush Nishant on 09/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit
import CoreLocation
final class StopDetailPopViewController: BaseViewController,PopoverStyle {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var stopDetailView: UIView!
    
    @IBOutlet weak var stopNameInfo: UILabel!
    @IBOutlet weak var stopNameValue: UILabel!
    @IBOutlet weak var cityNameInfo: UILabel!
    @IBOutlet weak var cityNameValue: UILabel!
    @IBOutlet weak var onStreetNameInfo: UILabel!
    
    @IBOutlet weak var onStreetValue: UILabel!
    @IBOutlet weak var atStreetInfo: UILabel!
    @IBOutlet weak var atStreetValue: UILabel!
    
    @IBOutlet weak var distanceNameInfo: UILabel!
    
    @IBOutlet weak var distanceValue: UILabel!
    @IBOutlet weak var wheelchairAccessinfo: UILabel!
    
    @IBOutlet weak var wheelChairAccessValue: UILabel!
    
    @IBOutlet weak var takemeThereButton: UIButton!
    
    @IBOutlet weak var busesButton: UIButton!
    
    @IBOutlet weak var liveBuses: UIButton!
    fileprivate var busesArviewController:ARViewController!
    var stop:Stop?
    var currentLocation:CLLocation?
    fileprivate var buses:[Bus]?{
        didSet{
            showBusUsingAr()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.activityIndicatorView.isViewHidden = true
        configureView()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateView()
    }
    
    @IBAction func takeMeThereAction(_ sender: UIButton) {
        guard let location = self.currentLocation,let stoplatitude = self.stop?.latitude,let stoplongitude = self.stop?.longitude else{
            let alert = UIAlertController(title: "Error", message: "Location is not set", preferredStyle:.alert)
            let action = UIAlertAction(title: "Ok", style:.cancel, handler: { [weak self](action) in
                self?.dismiss(animated: true, completion: nil)
            })
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        }
        let stringurl = "http://maps.apple.com/maps?saddr=\(location.coordinate.latitude),\(location.coordinate.longitude)&daddr=\(stoplatitude),\(stoplongitude)"
       UIApplication.shared.open(URL(string: stringurl)!, options: [:], completionHandler: nil)
    }
    @IBAction func busesAction(_ sender: UIButton) {
        goToBuslistController()
    }
    
    @IBAction func viewBusesLiveAction(_ sender: UIButton) {
        self.activityIndicatorView.isViewHidden = false
        self.activityIndicatorView.showText(withText: nil)
        getBuses()
    }
    private func goToBuslistController(){
        guard let stopnumber = self.stop?.stopNumber else {
            return
        }
        let busListvc = BuseListViewController.instantiate(withStoryBoard:StoryboardName.main.rawValue)
        busListvc.stopNumber = stopnumber
        self.navigationController?.pushViewController(busListvc, animated: true)
    }
    private func configureView(){
        let layer = stopDetailView.layer
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: stopDetailView.bounds , cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        cancelButton.layer.cornerRadius = 10
        takemeThereButton.layer.cornerRadius = 10
        busesButton.layer.cornerRadius = 10
        liveBuses.layer.cornerRadius = 10
    }
    private func updateView(){
        guard let stop = self.stop else{return}
        stopNameValue.text = stop.name
        cityNameValue.text = stop.city
        onStreetValue.text = stop.onStreet != nil ? stop.onStreet! : ""
        atStreetValue.text = stop.atStreet != nil ? stop.atStreet! : ""
        distanceValue.text = "\(String(describing: stop.distance != nil ? stop.distance! : 0)) m"
        wheelChairAccessValue.text =  stop.wheelChairAccess != nil ? stop.wheelChairAccess! : ""
    }
    private func showBusUsingAr(){
        busesArviewController = ARViewController()
        busesArviewController.dataSource = self
        busesArviewController.presenter.maxVisibleAnnotations = 30
        busesArviewController.presenter.presenterTransform = ARPresenterStackTransform()
        guard let buses = self.buses else{return}
        busesArviewController.setAnnotations(buses)
        busesArviewController.trackingManager.currentFilterLocation = CLLocation(coordinate: CLLocationCoordinate2D(latitude:(stop?.latitude)!, longitude:(stop?.longitude)!), altitude: 95, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp: Date())
        self.parent?.navigationController?.pushViewController(busesArviewController, animated: true)
    }
    private func getBuses(){
        guard let stopnumber = self.stop?.stopNumber else{return}
        TransitStopManager.getBuses(withStopNumber: stopnumber) { [weak self] (responseContainer) in
            if responseContainer.error != nil{
                let alertController = UIAlertController(title: "Server Error", message: responseContainer.error!.userInfo["error"] as? String, preferredStyle:.alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
                    self?.dismiss(animated: true, completion: nil)
                })
                alertController.addAction(cancelAction)
                self?.present(alertController, animated: true, completion: nil)
                self?.activityIndicatorView.isViewHidden = true
            }else{
                self?.buses = responseContainer.responseCollection
                self?.activityIndicatorView.isViewHidden = true
            }
        }
    }


}
extension StopDetailPopViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = BusAnnotation()
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        annotationView.updateUI()
        return annotationView
    }
}
extension StopDetailPopViewController: AnnotationViewDelegate {
    
    func didTouch(annotationView: ARAnnotationView) {
        let popOverVC = BusDetailViewController.instantiate(withStoryBoard: StoryboardName.main.rawValue)
        popOverVC.bus = annotationView.annotation as? Bus
        busesArviewController.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        busesArviewController.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
}

