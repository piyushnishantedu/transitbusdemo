//
//  BuseListViewController.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 09/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit

class BuseListViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var currentSelectedSegmentIndex = 0{
        didSet{
            if currentSelectedSegmentIndex == 0{
                guard self.scheduledBuses != nil else{
                    getNextBuses()
                    return
                }
                tableView.reloadData()
                activityIndicatorView.isViewHidden = true
            }else{
                guard self.buses != nil else{
                    getBuses()
                    return
                }
                tableView.reloadData()
                activityIndicatorView.isViewHidden = true
            }
        }
    }
    var stopNumber:Int?
    var buses:[Bus]?{
        didSet{
            tableView.reloadData()
        }
    }
    var scheduledBuses:[BusSchedule]?{
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicatorView.frame = CGRect(x: 0, y: 100, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.tableView.separatorStyle = .none
        currentSelectedSegmentIndex = 0
    }

    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        currentSelectedSegmentIndex = sender.selectedSegmentIndex
    }
    
    private func getBuses(){
        guard let stopnumber = stopNumber else{return}
        self.activityIndicatorView.isViewHidden = false
        self.activityIndicatorView.showText(withText: nil)
        TransitStopManager.getBuses(withStopNumber: stopnumber) { [weak self] (responseContainer) in
            if responseContainer.error != nil{
                //Code to show Error
                self?.activityIndicatorView.showText(withText:responseContainer.error!.userInfo["error"] as? String)
            }else{

                self?.buses = responseContainer.responseCollection
                self?.activityIndicatorView.isViewHidden = true
            }
        }
    }
    private func getNextBuses(){
        guard let stopnumber = stopNumber else{return}
        TransitStopManager.getSchedulesBus(withStopNumber:stopnumber) { [weak self] (responseContainer) in
            if responseContainer.error != nil{
            self?.activityIndicatorView.showText(withText:responseContainer.error!.userInfo["error"] as? String)
            }else{
                self?.scheduledBuses = responseContainer.responseCollection
                self?.activityIndicatorView.isViewHidden = true
            }
        }
    }

}
extension BuseListViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return currentSelectedSegmentIndex == 0 ? (scheduledBuses != nil ? scheduledBuses!.count : 0) : (buses != nil ? buses!.count : 0)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.currentSelectedSegmentIndex == 1 ? 200 : 260
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell = UITableViewCell()
        if currentSelectedSegmentIndex == 0{
            cell = tableView.dequeueReusableCell(withIdentifier:TableViewCellIdentifier.nextbuslistcell.rawValue, for:indexPath)
            let schedulesCell = cell as! NextBusTableViewCell
            guard let scheduledBus = self.scheduledBuses?[indexPath.row] else{return cell}
            schedulesCell.updateCellContent(scheduledBus:scheduledBus )
            cell = schedulesCell
        }else{
            cell  = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.buslistcell.rawValue, for: indexPath)
            let busTableViewCell = cell as! BusTableViewCell
            guard let bus = self.buses?[indexPath.row] else{return cell}
            busTableViewCell.updateCell(bus:bus)
            cell = busTableViewCell
        }
        
        
        return cell
    }
}
