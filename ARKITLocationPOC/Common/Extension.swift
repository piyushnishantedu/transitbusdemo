//
//  Extension.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 13/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
    class func instantiate(withStoryBoard:String) -> Self{
        return instantiateFromStoryboardHelper(storyboardName:withStoryBoard , storyboardId:String(describing: self))
    }
    private class func instantiateFromStoryboardHelper<T>(storyboardName: String, storyboardId: String) -> T
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: storyboardId) as! T
        return controller
    }
}
