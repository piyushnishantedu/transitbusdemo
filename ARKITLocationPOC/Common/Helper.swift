//
//  Helper.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 13/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
final class Helper{
    static func getAddressWith(latitdue:Double,longitude:Double,completionHandeler:@escaping(String)->Void){
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: latitdue, longitude: longitude)
        DispatchQueue.global(qos: .background).async{
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                var address = "NA"
                if error != nil {
                    completionHandeler(address)
                    
                } else {
                    if let placemarks = placemarks, let placemark = placemarks.first {
                        address = placemark.name ?? "NA"
                    } else {
                        address = "No Matching Addresses Found"
                    }
                    completionHandeler(address)
                }
            }
        }
        
    }

}
