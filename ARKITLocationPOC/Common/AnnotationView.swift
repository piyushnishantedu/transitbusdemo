//
//  AnnotationView.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 04/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit
class AnnotationView: ARAnnotationView {
    var titleLabel: UILabel?
    
    @IBOutlet var contentview: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    weak var delegate: AnnotationViewDelegate?
    var viewframe = CGRect(x: 0, y: 0, width: 90, height: 60)
    override init() {
        super.init(frame: viewframe)
        loadViewFromNib()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        //updateUI()
        viewframe = frame
        loadViewFromNib()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        //updateUI()
        loadViewFromNib()
    }
    private func loadViewFromNib(){
        Bundle.main.loadNibNamed("AnnotationView", owner: self, options:nil)
            addSubview(contentview)
        contentview.frame = viewframe//self.bounds//CGRect(x: 0, y: 0, width: 50, height: 50)
        contentview.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        updateUI()
    }
     func updateUI(){
        if let annotation = annotation as? Stop{
            titleLabel?.text = annotation.name
            distanceLabel?.text = String(format: "%.2f km", annotation.distanceFromUser / 1000)
        }else{
            distanceLabel?.text = ""
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.didTouch(annotationView: self)
    }
}

