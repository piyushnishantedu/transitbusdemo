//
//  Stop.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 06/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
final class Stop:ARAnnotation,ResponseObjectSerializable,Places{
    
    var distance: Double?
    let stopNumber:Int
    let name:String
    var bayNumber:String?
    var city:String?
    var onStreet:String?
    var atStreet:String?
    let latitude:Double
    let longitude:Double
    var wheelChairAccess:String?
    let routes:String
    var index:Int?
    init?(response: HTTPURLResponse, jsonObject:JSON) {
        stopNumber = jsonObject["StopNo"].intValue
        name = jsonObject["Name"].stringValue
        bayNumber = jsonObject["BayNo"].stringValue
        city = jsonObject["City"].stringValue
        onStreet = jsonObject["OnStreet"].stringValue
        atStreet = jsonObject["AtStreet"].stringValue
        latitude = jsonObject["Latitude"].doubleValue
        longitude = jsonObject["Longitude"].doubleValue
        wheelChairAccess = jsonObject["WheelchairAccess"].intValue == 1 ? "Yes":"No"
        distance = jsonObject["Distance"].doubleValue
        routes = jsonObject["Routes"].stringValue
        super.init(identifier: "", title: "", location: CLLocation(latitude: latitude, longitude: longitude))!
        //super.init()
        self.location = CLLocation(latitude: latitude, longitude: longitude)
        //self.init()
        /*let latitude = jsonObject["geometry"]["location"]["lat"].double!
        let longitude = jsonObject["geometry"]["location"]["lng"].double!
        reference = jsonObject["reference"].stringValue
        placeName = jsonObject["name"].stringValue
        address = jsonObject["vicinity"].stringValue
        
        super.init(identifier: "", title: "", location: CLLocation(latitude: latitude, longitude: longitude))!
        //super.init()
        self.location = CLLocation(latitude: latitude, longitude: longitude)*/
        
    }
    func setIndex(tag:Int){
        self.index = tag
    }
}
extension Stop:ResponseCollectionSerializable{
    static func collection(response: HTTPURLResponse, jsonObject: JSON) throws -> [Stop] {
        let places = jsonObject
        var stops = [Stop]()
        for i in 0..<places.count{
            let place = Stop(response: response, jsonObject: places[i])
            place?.setIndex(tag: i)
            stops.append(place!)
        }
        return stops
       /* return places.map({ Stop(response: response, jsonObject: $0.1 as JSON)!
        })*/
    }
}
