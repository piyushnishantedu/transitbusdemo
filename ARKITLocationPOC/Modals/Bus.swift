//
//  Bus.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 10/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation
final class Bus:ARAnnotation,ResponseObjectSerializable{
    let vehicleNo:String
    let tripId:UInt
    let routeNo:String
    let direction:String
    let destination:String
    let pattern:String
    let latitude:Double
    let longitude:Double
    let lastRecordedTime:String
    let geocoder = CLGeocoder()
    init?(response: HTTPURLResponse, jsonObject: JSON)  {
        vehicleNo = jsonObject["VehicleNo"].stringValue
        tripId = jsonObject["TripId"].uIntValue
        routeNo = jsonObject["RouteNo"].stringValue
        direction = jsonObject["Direction"].stringValue
        destination = jsonObject["Destination"].stringValue
        pattern = jsonObject["Pattern"].stringValue
        latitude = jsonObject["Latitude"].doubleValue
        longitude = jsonObject["Longitude"].doubleValue
        lastRecordedTime = jsonObject["RecordedTime"].stringValue
        super.init(identifier: "", title: "", location: CLLocation(latitude: latitude, longitude: longitude))!
        //super.init()
        self.location = CLLocation(latitude: latitude, longitude: longitude)
        //self.getAddressFromLatLong()
    }
   
}
extension Bus:ResponseCollectionSerializable{
    static func collection(response: HTTPURLResponse, jsonObject: JSON) throws -> [Bus] {
        let buseJson = jsonObject
        var buses = [Bus]()
        for i in 0..<buseJson.count{
            let bus = Bus(response: response, jsonObject: buseJson[i])
            //bus?.setIndex(tag: i)
            buses.append(bus!)
        }
        return buses
    }
}
