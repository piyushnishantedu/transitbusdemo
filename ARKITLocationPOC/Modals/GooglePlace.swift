//
//  GooglePlace.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation
final class GooglePlace:ARAnnotation,ResponseObjectSerializable,Places{
    var distance: Double?
    
    let name: String
    
    
    //var location:CLLocation
    let reference: String
    let address: String
    var phoneNumber: String?
    var website: String?
    var infoText: String {
        get {
            var info = "Address: \(address)"
            
            if phoneNumber != nil {
                info += "\nPhone: \(phoneNumber!)"
            }
            
            if website != nil {
                info += "\nweb: \(website!)"
            }
            return info
        }
    }
    override var description: String{
        return name
    }
    init?(response: HTTPURLResponse, jsonObject:JSON) {
        //self.init()
        let latitude = jsonObject["geometry"]["location"]["lat"].double!
        let longitude = jsonObject["geometry"]["location"]["lng"].double!
        reference = jsonObject["reference"].stringValue
        name = jsonObject["name"].stringValue
        address = jsonObject["vicinity"].stringValue
        
        super.init(identifier: "", title: "", location: CLLocation(latitude: latitude, longitude: longitude))!
        //super.init()
        self.location = CLLocation(latitude: latitude, longitude: longitude)
        
    }

}
extension GooglePlace:ResponseCollectionSerializable{
    static func collection(response: HTTPURLResponse, jsonObject: JSON) throws -> [GooglePlace] {
        let places = jsonObject["results"]
        return places.map({ GooglePlace(response: response, jsonObject: $0.1 as JSON)!
        })
    }
}

