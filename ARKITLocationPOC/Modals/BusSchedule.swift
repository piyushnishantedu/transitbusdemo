//
//  BusSchedule.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 10/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import SwiftyJSON
enum ScheduledStatus:String{
    case onTime = "On Time"
    case delay = "Delay"
    case none  = "NA"
    case ahead = "Ahead of schedule"
}
extension ScheduledStatus{
    init?(string:String) {
        switch string.lowercased() {
        case " ": self = .none
        case "+": self = .ahead
        case "*": self = .onTime
        case "-": self = .delay
        default: return nil
        }
        
    }
}
final class BusSchedule:ResponseObjectSerializable{
    let pattern:String
    let destination:String
    let expectedLeaveTime:String
    let remainingBusArrivalTime:Int
    let isCancelledTrip:Bool
    let isCancelledStop:Bool
    let lastUpdate:String
    let scheduleStatus:ScheduledStatus?
    //let routeName:String
    init?(response: HTTPURLResponse, jsonObject: JSON)  {
        pattern = jsonObject["Pattern"].stringValue
        destination = jsonObject["Destination"].stringValue
        expectedLeaveTime = jsonObject["ExpectedLeaveTime"].stringValue
        isCancelledTrip = jsonObject["CancelledTrip"].boolValue
        isCancelledStop = jsonObject["CancelledStop"].boolValue
        lastUpdate = jsonObject["LastUpdate"].stringValue
        remainingBusArrivalTime = jsonObject["ExpectedCountdown"].intValue
        scheduleStatus  = ScheduledStatus(string: jsonObject["ScheduleStatus"].stringValue)
        
    }
    
    
}
extension BusSchedule:ResponseCollectionSerializable{
    static func collection(response: HTTPURLResponse, jsonObject: JSON) throws -> [BusSchedule] {
        let buseJson = jsonObject
        var buses = [BusSchedule]()
        for i in 0..<buseJson.count{
            for j in 0..<buseJson[i]["Schedules"].count{
                let bus = BusSchedule(response: response, jsonObject: buseJson[i]["Schedules"][j])
                buses.append(bus!)
            }
            
        }
        return buses
    }
    
    
}
