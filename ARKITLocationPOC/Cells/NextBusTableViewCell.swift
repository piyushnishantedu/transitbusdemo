//
//  NextBusTableViewCell.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 10/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit

class NextBusTableViewCell: UITableViewCell {

    @IBOutlet weak var destinationInfo: UILabel!
    
    @IBOutlet weak var destination: UILabel!
    
    @IBOutlet weak var expectedDepartureInfo: UILabel!
    
    @IBOutlet weak var expectedDeparture: UILabel!
    
    @IBOutlet weak var remainingTimetoReachInfo: UILabel!
    
    @IBOutlet weak var remainingTimetoReac: UILabel!
    
    @IBOutlet weak var statusInfo: UILabel!
    
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var cancelStopInfo: UILabel!
    
    @IBOutlet weak var cancelStop: UILabel!
    
    @IBOutlet weak var tripStatusinfo: UILabel!
    
    @IBOutlet weak var tripStatus: UILabel!
    
    @IBOutlet weak var lastUpdateInfo: UILabel!
    
    @IBOutlet weak var lastUpdate: UILabel!
    @IBOutlet weak var cellContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCellStyle()
    }
    private func configureCellStyle(){
        cellContentView.backgroundColor = UIColor.white
        self.contentView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        cellContentView.layer.cornerRadius = 5
        cellContentView.layer.masksToBounds = false
        cellContentView.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        cellContentView.layer.shadowOffset = CGSize.zero
        cellContentView.layer.shadowOpacity = 0.8
        
    }
    func updateCellContent(scheduledBus:BusSchedule){
        destination.text = scheduledBus.destination
        expectedDeparture.text = scheduledBus.expectedLeaveTime
        remainingTimetoReac.text = "\(scheduledBus.remainingBusArrivalTime) Minutes"
        status.text = scheduledBus.scheduleStatus?.rawValue
        cancelStop.text = !scheduledBus.isCancelledStop ? "Yes" : "No"
        tripStatus.text = !scheduledBus.isCancelledTrip ? "Online" : "Offline"
        lastUpdate.text = scheduledBus.lastUpdate
    }

}
