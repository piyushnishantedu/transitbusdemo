//
//  BusTableViewCell.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 10/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit

class BusTableViewCell: UITableViewCell {

    @IBOutlet weak var busNumberInfo: UILabel!
    
    @IBOutlet weak var busNumber: UILabel!
    
    @IBOutlet weak var directionInfo: UILabel!
   
    @IBOutlet weak var direction: UILabel!
    
    @IBOutlet weak var destinationInfo: UILabel!
    
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var lastAddressinfo: UILabel!
    
    @IBOutlet weak var lastAddress: UILabel!
    @IBOutlet weak var lastUpdatedInfo: UILabel!
    
    @IBOutlet weak var lastUpdated: UILabel!
    @IBOutlet weak var cellContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCellStyle()
    }
    private func configureCellStyle(){
        /*let cornerRadius: CGFloat = 8
        let shadowOffsetWidth: Int = 0
        let shadowOffsetHeight: Int = 0
        let shadowColor: UIColor? = UIColor.black
        let shadowOpacity: Float = 0.7
        let layer = cellContentView.layer
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: cellContentView.bounds , cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        layer.shouldRasterize = true*/
        cellContentView.backgroundColor = UIColor.white
        self.contentView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        cellContentView.layer.cornerRadius = 5
        cellContentView.layer.masksToBounds = false
        cellContentView.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        cellContentView.layer.shadowOffset = CGSize.zero
        cellContentView.layer.shadowOpacity = 0.8
        
    }
    func updateCell(bus:Bus){
        busNumber.text = bus.vehicleNo
        direction.text = bus.direction
        destination.text = bus.destination
        lastAddress.text = ""
        lastUpdated.text = bus.lastRecordedTime
        if lastAddress.text!.isEmpty {
            Helper.getAddressWith(latitdue: bus.latitude, longitude: bus.longitude, completionHandeler: { [weak self] (address) in
                self?.lastAddress.text = address
            })
        }
        
    }


}
