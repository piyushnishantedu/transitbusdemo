//
//  TransitStopsManager.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 06/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//
import Foundation
import CoreLocation
final class TransitStopManager:ApiBaseManager{
    static func getStops(withLocation location:CLLocation,radius:Int = 2000,completionHandler: @escaping (ResponseContainer<Stop>) -> Void){
        let action = ApiAction.getStops(location: location, radius: radius)
        apiDataModal.setUrlString(urlstring: action.urlPath)
        //apiDataModal.header = ["Accept":"application/JSON"]
        ApiCallHandeler.httpGetRequestCollection(withDataSource: apiDataModal) { (response:ResponseContainer<Stop>) in
            completionHandler(response)
            
        }
    }
    static func getBuses(withStopNumber stopNumber:Int,completionHandler: @escaping (ResponseContainer<Bus>) -> Void){
        let action = ApiAction.getBuses(stopNumber: stopNumber)
        apiDataModal.setUrlString(urlstring: action.urlPath)
        ApiCallHandeler.httpGetRequestCollection(withDataSource: apiDataModal) { (response:ResponseContainer<Bus>) in
            completionHandler(response)
        }
    }
    static func getSchedulesBus(withStopNumber stopNumber:Int,completionHandler: @escaping (ResponseContainer<BusSchedule>) -> Void){
        let action = ApiAction.getNextScheduledBu(stopNumber:stopNumber )
        apiDataModal.setUrlString(urlstring: action.urlPath)
        ApiCallHandeler.httpGetRequestCollection(withDataSource:apiDataModal ) { (response:ResponseContainer<BusSchedule>) in
            completionHandler(response)
        
        }
        
    }
}
