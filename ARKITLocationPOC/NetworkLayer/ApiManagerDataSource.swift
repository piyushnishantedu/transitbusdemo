//
//  ApiManagerDataSource.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
//MARK: - Protocol for ApimanagerModal
 protocol ApiManagerDataSource {
     var params:Parameters?{get set}
     var header:HTTPHeaders?{get set}
     var urlString:String?{get set}
     var jsonencoding:JSONEncoding{get}
}
//MARK: - Api Manager Modal Class
final class ApiDataModal:ApiManagerDataSource {
       var params: Parameters?
       var header: HTTPHeaders?
       var urlString: String?
       var body:String?
    
    static let sharedInstance = ApiDataModal()
    init() {
        header = ["Accept":"application/JSON"]
    }
    func resetAllValues() {
        self.params = nil
        self.header = nil
        urlString = nil
        body = nil
    }
    
    //MARK: - Set url string
    /**
     - parameter urlstring: String reference
     - returns : No value is return
     */
    func setUrlString(urlstring:String){
        self.urlString = urlstring
    }
    //MARK: - Set header
    /**
     - parameter header: Dictionary with string key and value is anyobject
     - returns : No value is return
     */
    final func setHeader(header:HTTPHeaders){
        //Remove header content if header is not nil
        if var header = self.header {
            if header.count > 0{
                header.removeAll()
            }
        }else{
            self.header = HTTPHeaders()
        }
        let timestamp = String(NSDate().timeIntervalSince1970)
        let uuid = NSUUID().uuidString // to disambiguate concurrent requests
        let requestId = "\(timestamp)-\(uuid)"
        self.header?["X-Request-ID"] = requestId
        for(key,value) in header{
            self.header?[key] = value
        }
        
    }
    //MARK: - Set params
    /**
     - parameter param: Dictionary with string key and value is anyobject
     - returns : No value is return
     */
    final func setParam(param:Parameters){
        self.params = param
    }
}
//Extension for APiManager Datasource
 extension ApiManagerDataSource{
    typealias success = (JSON)->Void
    typealias failure = (Error)->Void
    
    var jsonencoding:JSONEncoding{
        return JSONEncoding.default
    }
    var header : HTTPHeaders{
        return ["Accept":"application/JSON"]
    }
    var urlencoding:URLEncoding{
        return URLEncoding.default
    }
}
