//
//  ApiCallHandler.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire
import SwiftyJSON
final class ApiCallHandeler{
    private static var Manager: Alamofire.SessionManager = {
        
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    static func httpGetrequestOfObject<T:ApiManagerDataSource,U:ResponseObjectSerializable>(withDataSource dataSource:T,completionHandler:@escaping (ResponseContainer<U>) -> Void){
        _ = Manager.request(dataSource.urlString!, method: .get, headers: dataSource.header).responseJSON(completionHandler: { (response) in
            let result:ResponseContainer<U> = JSONParser.responseObject(response: response)
            completionHandler(result)
        })
    }
    static func httpGetRequestCollection<T:ApiManagerDataSource,U:ResponseObjectSerializable>(withDataSource dataSource:T,completionHandler:@escaping (ResponseContainer<U>) -> Void){
       _ = Manager.request(dataSource.urlString!, method: .get, headers: dataSource.header).responseJSON(completionHandler: { (response) in
        let result:ResponseContainer<U> = JSONParser.responseCollection(response: response)
        completionHandler(result)
        })
    }
}

