//
//  GooglePlaceApiManager.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import CoreLocation
final class GooglePlaceApiManager:ApiBaseManager{
     static func getGooglePlace(withLocation location:CLLocation,radius:Int = 30,completionHandler: @escaping (ResponseContainer<GooglePlace>) -> Void){
        let action = ApiAction.getGooglePlaces(location: location, radius: radius)
        apiDataModal.setUrlString(urlstring: action.urlPath)
        ApiCallHandeler.httpGetrequestOfObject(withDataSource: apiDataModal) { (response:ResponseContainer<GooglePlace>) in
                completionHandler(response)
        }
    }
    static func getGooglePlaces(withLocation location:CLLocation,radius:Int = 2000,completionHandler: @escaping (ResponseContainer<GooglePlace>) -> Void){
        //httpGetRequestCollection
        let action = ApiAction.getGooglePlaces(location: location, radius: radius)
        apiDataModal.setUrlString(urlstring: action.urlPath)
        ApiCallHandeler.httpGetRequestCollection(withDataSource: apiDataModal) { (response:ResponseContainer<GooglePlace>) in
            completionHandler(response)
            
        }
    }
}
