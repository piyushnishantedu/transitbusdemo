//
//  ApiEnum.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import CoreLocation
let apiURL = "https://maps.googleapis.com/maps/api/place/"
let apiKey = ""
let transitApiKey = "HsXt7Ic4hD3uIjPXWTlB"
let baseApiUrl = "http://api.translink.ca/"
enum ApiAction{
    case getGooglePlaces(location: CLLocation, radius: Int)
    case getStops(location:CLLocation, radius:Int)
    case getBuses(stopNumber:Int)
    case getNextScheduledBu(stopNumber:Int)
    var urlPath:String{
        switch self {
        case .getGooglePlaces(let location,let radius):
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            let uri = apiURL + "nearbysearch/json?location=\(latitude),\(longitude)&radius=\(radius)&sensor=true&types=establishment&key=\(apiKey)"
            return uri
        case .getStops(let location, let radius):
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            let uri = baseApiUrl + "rttiapi/v1/stops?apikey=\(transitApiKey)&lat=\(latitude)&long=\(-longitude)&radius=\(radius)"
            return uri
        case .getBuses(let stopNumber) :
            let uri = baseApiUrl + "rttiapi/v1/buses?apikey=\(transitApiKey)&stopNo=\(stopNumber)"
            return uri
        case .getNextScheduledBu(let stopNUmber) :
            let uri = baseApiUrl + "rttiapi/v1/stops/\(stopNUmber)/estimates?apikey=\(transitApiKey)"
            return uri
        }
    }
}
