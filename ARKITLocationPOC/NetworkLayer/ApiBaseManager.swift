//
//  ApiBaseManager.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
 class ApiBaseManager{
    //Variables
    /*internal lazy var apiDataModal:ApiDataModal = {
        return ApiDataModal.sharedInstance
    }()*/
    static let apiDataModal = ApiDataModal.sharedInstance
    //MARK:Get param
    /**
     - parameter apiaction : apiAction reference
     - returns : no value is return
     */
    internal final func getParam(apiaction:ApiAction) -> [String:Any] {
        switch apiaction {
        default : return[:]
        }
    }
}
