//
//  ResponseObjectSerializable.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
fileprivate enum ErrorCode:Int {
    case ErrorCodeServerIssue = -6000
}
enum ServerErrorCode:String{
    case dbError = "Database connection error"
    case stopNotvalid = "Invalid stop number"
    case stopNotFound  = "Stop number not found"
    case unknownEstimateError = "Unknown get estimates error"
    case invalidRoute = "Invalid route"
    case notEstimateStopFound = "No stop estimates found"
    case invalidTimeFrame = "Invalid time frame"
    case invalidCount = "Invalid count"
    case invalideBusNumber = "Invalid Bus number"
    case busnumberNotFound = "Bus Number Not found"
    case unknowngetBusError = "Unknown Error"
    case noBusesFound = "No Buses Found"
    case unknowngetbusesBystop = "Unknown get buses by stop error"
    case unknowngetbusesByroute = "Unknown get buses by route error"
    case invalidStopnumber = "invalid Stop number"
    case invalidroutenumber = "Invalid route number"
    case stopNumberNotFOund = "Stop  not found"
    case unknowngetbusesbystopandrouteerror = "Unknown get buses by stop and route error"
    case invalidStopNumber = "Invalid Stop Number"
    case unknownStopCheckError = "Unknown Stop Check Error"
    case unkonwngetStopError = "Unknown get Stop error"
    case invalidLatLng = "Invalid latitude longitude"
    case noSTopFound = "No Stop Found"
    case unknowngetSTopsError = "Unknown get Stops error"
    case radiusLager = "Radius is too large"
}
extension ServerErrorCode{
    init?(code:Int) {
        switch code {
        case 10002: self = .dbError
        case 3001: self = .stopNotvalid
        case 3002: self = .stopNotFound
        case 3003: self = .unknownEstimateError
        case 3004 : self = .invalidRoute
        case 3005: self = .notEstimateStopFound
        case 3006: self = .invalidTimeFrame
        case 3007 : self = .invalidCount
        case 2001 : self = .invalideBusNumber
        case 2002 : self = .busnumberNotFound
        case 2003 : self = .unknowngetBusError
        case 2011 : self = .noBusesFound
        case 2012 : self = .unknowngetbusesBystop
        case 2013 : self = .unknowngetbusesByroute
        case 2014 : self = .invalidStopnumber
        case 2015 : self = .invalidroutenumber
        case 2016,1002 : self = .stopNumberNotFOund
        case 2018 : self = .unknowngetbusesbystopandrouteerror
        case 1001 : self = .invalidStopNumber
        case 1003 : self = .unknownStopCheckError
        case 1004 : self = .unkonwngetStopError
        case 1011 : self = .invalidLatLng
        case 1012 : self = .noSTopFound
        case 1013 : self = .unknowngetSTopsError
        case 1014 : self = .radiusLager
        default: return nil
        }
        
    }
}
struct ResponseContainer<T: ResponseCollectionSerializable> {
    var error:NSError?
    var responseCollection:Array<T>?
    var responseObject:T?
}
class JSONParser {
    
    public static func parseResult(result:Result<Any>) -> NSError? {
        var error:NSError?
        switch result {
        case .success(let data):
            let json = JSON(data)
            
            if let errorDict = json["error"].dictionary {
                //TODO: Check more
                let message = errorDict["userMessage"]?.rawString()
                let userInfo = [NSLocalizedDescriptionKey: message]
                error = NSError(domain: "com.ARKITDEMO.error", code: (errorDict["code"]?.int)!, userInfo: userInfo)
            } /*else if let errorString = json["error"].string {
                let userInfo = [NSLocalizedDescriptionKey: errorString]
                error = NSError(domain: "com.ARKITDEMO.error", code: ErrorCode.ErrorCodeServerIssue.rawValue, userInfo: userInfo)
            }*/
            else{
                if let serverError = ServerErrorCode(code:json["Code"].intValue ){
                    let userInfo = ["error": serverError.rawValue]
                    error = NSError(domain: "com.ARKITDEMO.error", code: ErrorCode.ErrorCodeServerIssue.rawValue, userInfo: userInfo)
                }
            }
            
        case .failure(let err):
            print("Request failed with error: \(err)")
            error = err as NSError?
        }
        
        return error
    }
    
    public static func responseCollection<T: ResponseCollectionSerializable>(response:Alamofire.DataResponse<Any>) -> ResponseContainer<T> {
        var responseContainer = ResponseContainer<T>()
        let error = JSONParser.parseResult(result: response.result)
        
        if error != nil  {
            responseContainer.error = error
        } else {
            let jsonObject = JSON.init(parseJSON: String.init(data: response.data!, encoding: .utf8)!)
            do {
                responseContainer.responseCollection = try T.collection(response: response.response!, jsonObject: jsonObject)
            } catch let error {
                print(error)
            }
            
        }
        return responseContainer
    }
    
    public static func responseObject<T: ResponseObjectSerializable>(response:Alamofire.DataResponse<Any>) -> ResponseContainer<T> {
        var responseContainer = ResponseContainer<T>()
        let error = JSONParser.parseResult(result: response.result)
        if error != nil  {
            responseContainer.error = error
        } else {
            let jsonObject = JSON.init(parseJSON: String.init(data: response.data!, encoding: .utf8)!)
            do {
                responseContainer.responseObject = try T.init(response: response.response!, jsonObject: jsonObject)
            } catch let error {
                print(error)
            }
            
        }
        return responseContainer
    }
}
