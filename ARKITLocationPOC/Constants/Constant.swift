//
//  Constant.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 13/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
enum StoryboardName:String{
    case main = "Main"
}
enum ViewControllerIdentifier:String{
    case stopDetailPopViewController = "StopDetailPopViewController"
    case homeViewController = "HomeViewController"
    case buseListViewController = "BuseListViewController"
    case busDetailViewController = "BusDetailViewController"
}
enum TableViewCellIdentifier:String{
    case nextbuslistcell = "nextbuslistcell"
    case buslistcell = "buslistcell"
}
