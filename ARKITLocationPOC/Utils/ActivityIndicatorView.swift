//
//  ActivityIndicatorView.swift
//  POPPOC
//
//  Created by Piyush Nishant on 12/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import UIKit

class ActivityIndicatorView: UIView {

    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var view: UIView!
    var isViewHidden = false{
        didSet{
            self.alpha = isViewHidden ? 0 : 1
            activityIndicatorView.stopAnimating()
            
        }
    }
    private var indicatorAlpha = 0{
        didSet{
            if indicatorAlpha == 0 {
                activityIndicatorView.stopAnimating()
            }else{
                activityIndicatorView.startAnimating()
            }
            activityIndicatorView.alpha = CGFloat(indicatorAlpha)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    private func setup() {
        
        view = loadViewFromNib()
        view.frame = bounds
        
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth,
                                 UIViewAutoresizing.flexibleHeight]
        
        addSubview(view)
        
        // Add our border here and every custom setup
        
    }
   private func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    func setBackgroundColor(color:UIColor = UIColor.blue){
        self.view.backgroundColor = color
    }
    func showText(withText text:String?){
        guard let message = text else{
            messageLabel.text = ""
            indicatorAlpha = 1
            return
        }
        messageLabel.text = message
        indicatorAlpha = 0
    }
    
}
