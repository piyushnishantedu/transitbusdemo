//
//  NetworkLayerProtocol.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 01/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import SwiftyJSON
public protocol ResponseObjectSerializable {
    init?(response: HTTPURLResponse, jsonObject: JSON) throws
}

public protocol ResponseCollectionSerializable:ResponseObjectSerializable {
    static func collection(response: HTTPURLResponse, jsonObject: JSON) throws -> [Self]
}

enum SerializationError: Error {
    case missing(String)
}
