//
//  CommonProtocol.swift
//  ARKITLocationPOC
//
//  Created by Piyush Nishant on 04/03/18.
//  Copyright © 2018 Piyush Nishant. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
protocol AnnotationViewDelegate:class {
    func didTouch(annotationView: ARAnnotationView)
}
protocol Places {
    var name:String{get}
    var distance:Double?{get}
}
protocol PopoverStyle {
    var cornerRadius: CGFloat{get}
    var shadowOffsetWidth: Int{get}
    var shadowOffsetHeight: Int{get}
    var shadowColor: UIColor?{get set}
    var shadowOpacity: Float{get}
}
extension PopoverStyle{
    var cornerRadius:CGFloat{
        return  8
    }
    var shadowOffsetWidth:Int{
        return 0
    }
    var shadowOffsetHeight:Int{
        return 3
    }
    var shadowColor:UIColor?{
        get{
            return UIColor.black
        }set{
            shadowColor = newValue
        }
    }
    var shadowOpacity:Float{
        return 0.5
    }
}

